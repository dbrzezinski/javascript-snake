## Javascript Snake

Kleines Spiel welches auf Javascript Basis funktioniert und dabei alle grundlegenden Funktionen sowie Erweiterungen wie das laufen durch die Wände mitbringt

![Javascript Snake Screenshot](./assets/screenshot.jpg)

#### Kernfunktionalitäten
1. Bewegen der Schlange mit den Pfeiltasten
2. Pausieren und fortführen des Spiels
3. Erhöhen der Spielgeschwindigkeit mit jeder gesammelten Frucht
4. Laufen durch Wände möglich. Die Schlange wird entsprechend aufgeteilt an beiden Rändern gezeigt
5. Spiel gilt als verloren, wenn die schlange sich selbst beist, auch dann, wenn diese nach rechts läuft und man links drückt. Es wird auf Kollision bei jeder Bewegung geprüft


Viele Grüße
Daniel Brzezinski

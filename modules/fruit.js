import {matchfieldObject} from "./matchfield";

console.log('fruit module loaded')
import * as matchfield from './matchfield'

let fruitColors = [ 'orange', 'green', 'coral', 'gold', 'lime', 'magenta', 'olive', 'seaGreen']

export const fruitObject = {
    x: 0,
    y: 0,
    width: 20,
    height: 20,
    color: ''
}

/**
 * Draw fruit with latest position. E.g. used to (re-)draw the fruit in interval
 */
export function drawFruit() {
    matchfield.matchfieldObject.drawCtx.fillStyle = fruitObject.color
    matchfield.matchfieldObject.drawCtx.fillRect( fruitObject.x, fruitObject.y, fruitObject.width, fruitObject.height)
}

/**
 * Draw fruit with new coordinates
 */
export function drawNewFruit() {
    // get random fruit coordinates
    const randomMatchField = matchfield.getRandomMatchFieldBlockPosition()
    fruitObject.x = randomMatchField.x
    fruitObject.y = randomMatchField.y

    // get random fruit color
    fruitObject.color = fruitColors[Math.floor(Math.random() * fruitColors.length)]
    this.drawFruit();
}
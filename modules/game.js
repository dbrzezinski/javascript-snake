import {SNAKE_DIRECTIONS} from "./snake";

console.log('game module loaded')

import * as matchfield from './matchfield'

import * as snake from './snake'
import * as fruit from './fruit'

// counter for eaten fruits/points
export let totalPoints

// decrease the value to speed up the game
export let gameSpeed

// value for the user display. will be increased by one every time
let gameSpeedUser

let gameInterval = null

let customEvents = {
    points: {
        eventObj: null,
        eventTarget: document.getElementsByClassName("points")[0]
    },
    gamespeed: {
        eventObj: null,
        eventTarget: document.getElementsByClassName("gamespeed")[0]
    }
}

export function initGame() {
    if(!gameInterval) {

        totalPoints = 0
        gameSpeed = 70
        gameSpeedUser = 1

        matchfield.initMatchfield()
        snake.initSnake()
        fruit.drawNewFruit()

        document.addEventListener('keydown', keyPushed)
        document.addEventListener('gameOver', () => endGame() )

        customEvents.points.eventTarget.addEventListener('pointsUpdated', (evt) => evt.target.innerHTML = evt.detail.points )
        customEvents.gamespeed.eventTarget.addEventListener('gamespeedUpdated', (evt) => evt.target.innerHTML = evt.detail.gameSpeed )

        customEvents.points.eventObj = new CustomEvent('pointsUpdated', {detail: {points: totalPoints }})
        customEvents.points.eventTarget.dispatchEvent(customEvents.points.eventObj)

        customEvents.gamespeed.eventObj = new CustomEvent('gamespeedUpdated', {detail: {gameSpeed: gameSpeedUser }} )
        customEvents.gamespeed.eventTarget.dispatchEvent(customEvents.gamespeed.eventObj)


        gameInterval = window.setInterval(startGame, gameSpeed)
    }
}

export function addPoint() {
    totalPoints++
    increaseGameSpeed()

    // dispatch event to update points on display
    customEvents.points.eventObj = new CustomEvent('pointsUpdated', {detail: {points: totalPoints }})
    customEvents.points.eventTarget.dispatchEvent(customEvents.points.eventObj)
}

/**
 * increase game speed. e.g. after eaten fruit for more difficult
 * @param increase
 */
function increaseGameSpeed(increase = 2) {
    gameSpeed -= increase
    gameSpeedUser++

    // stop interval and create with new speed
    window.clearInterval(gameInterval)
    gameInterval = window.setInterval(startGame, gameSpeed)

    // dispatch event to update gamespeed on display
    customEvents.gamespeed.eventObj = new CustomEvent('gamespeedUpdated', {detail: {gameSpeed: gameSpeedUser }} )
    customEvents.gamespeed.eventTarget.dispatchEvent(customEvents.gamespeed.eventObj)
}

/**
 * Get initial elements and draw
 */
function startGame() {
    matchfield.initMatchfield()
    snake.drawSnake()
    fruit.drawFruit()
}

function endGame() {
    totalPoints = 0
    window.clearInterval(gameInterval)
    gameInterval = null
    document.getElementById('')

    initGame()
}

function pauseToggleGame() {

    // pause game
    if(gameInterval) {
        window.clearInterval(gameInterval)
        gameInterval = null
        // continue game
    } else {
        gameInterval = window.setInterval(startGame, gameSpeed)
    }

}


function keyPushed(evt) {

    // prevent scrolling by clicking the arrow keys
    evt.preventDefault()

    switch(evt.key) {
        case 'Escape':
            endGame()
            break
        case 'p':
            pauseToggleGame()
            break
        case 'ArrowLeft':
            snake.changeSnakeDirectory(SNAKE_DIRECTIONS.LEFT)
            break
        case 'ArrowUp':
            snake.changeSnakeDirectory(SNAKE_DIRECTIONS.UP)
            break
        case 'ArrowRight':
            snake.changeSnakeDirectory(SNAKE_DIRECTIONS.RIGHT)
            break
        case 'ArrowDown':
            snake.changeSnakeDirectory(SNAKE_DIRECTIONS.DOWN)
            break
    }
}

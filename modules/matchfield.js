console.log('matchfield module loaded')

const matchfieldID = 'matchfield';

const blockSize = 20

export let matchfieldObject = {
    width: 800,
    height: 800,
    stepsX: 0,
    stepsY: 0,
    color: "black",
    borderWidth: 0,
    borderColor: "#666",
    drawCtx: null
}

// Context for our canvas object
export let matchfieldCanvas = null

/**
 * create basic matchfield
 */
export function initMatchfield() {
    matchfieldCanvas = document.getElementById(matchfieldID)
    matchfieldCanvas.setAttribute('width', matchfieldObject.width)
    matchfieldCanvas.setAttribute('height', matchfieldObject.height)
    matchfieldObject.stepsX = (matchfieldObject.width - matchfieldObject.borderWidth * 2) / blockSize
    matchfieldObject.stepsY = (matchfieldObject.height - matchfieldObject.borderWidth * 2) / blockSize

    matchfieldObject.drawCtx = matchfieldCanvas.getContext('2d')

    drawMatchfieldBorder()
    drawMatchfield()
}

/**
 * Get a random valid position of the matchfield
 * @param size number of a blocksize
 * @returns {{x: number, y: number}}
 */
export function getRandomMatchFieldBlockPosition(size) {
    size = size || blockSize
    return {
        x: Math.floor(Math.random() * matchfieldObject.stepsX) * size + matchfieldObject.borderWidth,
        y: Math.floor(Math.random() * matchfieldObject.stepsY) * size + matchfieldObject.borderWidth
    }
}


/**
 * Draw the borders of the matchfield
 */
function drawMatchfieldBorder() {
    matchfieldObject.drawCtx.fillStyle = matchfieldObject.borderColor
    matchfieldObject.drawCtx.fillRect(0, 0, matchfieldObject.width , matchfieldObject.height)
}

/**
 * Draw just the matchfield
 */
function drawMatchfield() {
    // Fill the path
    matchfieldObject.drawCtx.fillStyle = matchfieldObject.color
    matchfieldObject.drawCtx.fillRect(
        matchfieldObject.borderWidth,
        matchfieldObject.borderWidth,
        matchfieldObject.width - matchfieldObject.borderWidth * 2,
        matchfieldObject.height - matchfieldObject.borderWidth * 2
    )
}
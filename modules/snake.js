import {totalPoints} from "./game";

console.log('snake module loaded')

import * as matchfield from './matchfield'
import * as fruit from './fruit'
import * as game from './game'

// variables for our snake (will be initialied in the method underneath
let snakeTail, initialSnakeLength, snakeObject
export let snakeDirectory

// the possible directions for a snake move
export const SNAKE_DIRECTIONS = {
    LEFT: 1,
    UP: 2,
    RIGHT: 3,
    DOWN: 4
}

export function initSnake() {
    snakeTail = []
    initialSnakeLength = 4

    snakeDirectory = -1

    snakeObject = {
        x : 0,
        y : 0,
        width: 20,
        height: 20,
        snakeColor: '#0af',
        snakeBorder: 2,
        snakeBorderColor: '#000'
    }
}

/**
 * Move the snake to the new position, check if we have a collision or catched a fruit
 */
export function drawSnake() {
    moveSnake()

    if(isCollision()) {
        document.dispatchEvent(new CustomEvent('gameOver'))
    }

    updateSnakeTrail()

    if(catchedFruit()) {
        updateSnakeTrail(true)
        fruit.drawNewFruit()
    }

    drawSnakeElements()
}

/**
 * Add new snake item if needed and delete the last one after a move
 * @param newItem
 */
function updateSnakeTrail(newItem = false) {

    if (snakeDirectory < 0) return

    if(snakeTail.length < initialSnakeLength) {
        let newTailItem = {x: snakeObject.x, y: snakeObject.y}
        snakeTail.push(newTailItem)
        drawSnakeElements(newTailItem)
    } else {
        // remove last position, but just if we don't eat a fruite, otherwise this is the new item
        if(newItem !== true) {
            snakeTail.shift();
        }
    }

    // Push current position
    snakeTail.push({x: snakeObject.x, y: snakeObject.y})
}

/**
 * Draw a piece of a snake tail
 * @param newTailItem {{x: number, y: number}}
 */
function drawSnakeElements(newTailItem) {

    let posX, posY
    if(newTailItem) {
        posX = newTailItem.x
        posY = newTailItem.y
    } else {
        posX = snakeObject.x
        posY = snakeObject.y
    }

    for(let i = 0; i < snakeTail.length; i++) {
        // check for matchfield dimensions
        if(snakeTail[i].x + snakeObject.width >= matchfield.matchfieldObject.width + snakeObject.width) {
            snakeTail[i].x = 0
            snakeObject.x = 0
        } else if(snakeTail[i].y + snakeObject.height >= matchfield.matchfieldObject.height + snakeObject.height) {
            snakeTail[i].y = 0
            snakeObject.y = 0
        } else if(snakeTail[i].x < 0) {
            snakeTail[i].x = snakeObject.x = matchfield.matchfieldObject.width - snakeObject.width
        } else if(snakeTail[i].y < 0) {
            snakeTail[i].y = snakeObject.y = matchfield.matchfieldObject.height - snakeObject.height
        }

        // draw snake body
        matchfield.matchfieldObject.drawCtx.fillStyle = snakeObject.snakeColor
        matchfield.matchfieldObject.drawCtx.fillRect( snakeTail[i].x, snakeTail[i].y, snakeObject.width, snakeObject.height)

        // draw Border
        matchfield.matchfieldObject.drawCtx.strokeStyle = snakeObject.snakeBorderColor
        matchfield.matchfieldObject.drawCtx.lineWidth = snakeObject.snakeBorder
        matchfield.matchfieldObject.drawCtx.rect( snakeTail[i].x, snakeTail[i].y, snakeObject.width +2, snakeObject.height)
        matchfield.matchfieldObject.drawCtx.stroke()
    }

}

/**
 * Check if the current position is the same like of a fruit and add a point
 * @returns {boolean}
 */
function catchedFruit() {
    if (snakeObject.x === fruit.fruitObject.x && snakeObject.y === fruit.fruitObject.y) {
        game.addPoint()
        return true
    }
    return false
}

/**
 * check if the snake bite themself
 * @returns {boolean}
 */
function isCollision() {
    return (snakeTail.find(tail => tail.x === snakeObject.x && tail.y === snakeObject.y) !== undefined)
}

/**
 * change move direction for the snake.
 * @param directory - if you enter -1 the snake will not move anymore.
 */
export function changeSnakeDirectory(directory) {
    if(Object.keys(SNAKE_DIRECTIONS).indexOf(directory) || directory === -1)
        snakeDirectory = directory
}

/**
 * Move snake to next position depends on direction
 */
export function moveSnake() {
    switch(snakeDirectory) {
        case SNAKE_DIRECTIONS.LEFT:
            snakeObject.x -= snakeObject.width
            break
        case SNAKE_DIRECTIONS.UP:
            snakeObject.y -= snakeObject.height
            break
        case SNAKE_DIRECTIONS.RIGHT:
            snakeObject.x += snakeObject.width
            break
        case SNAKE_DIRECTIONS.DOWN:
            snakeObject.y += snakeObject.height
            break
    }
}